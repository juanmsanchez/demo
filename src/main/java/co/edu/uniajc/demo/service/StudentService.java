package co.edu.uniajc.demo.service;

import co.edu.uniajc.demo.Model.StudentModel;
import co.edu.uniajc.demo.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StudentService {

    private final StudentRepository studentRepository;

    @Autowired
    public StudentService(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    public StudentModel createStudent (StudentModel studentModel){
        return studentRepository.save(studentModel);
    }

    public StudentModel updateStudent (StudentModel studentModel){
        return studentRepository.save(studentModel);
    }



}
