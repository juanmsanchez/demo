package co.edu.uniajc.demo.controller;

import co.edu.uniajc.demo.Model.StudentModel;
import co.edu.uniajc.demo.service.StudentService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/student")
@Api("Students")
public class StudentController {

    private StudentService studentService;

    @Autowired
    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    @PostMapping(path = "/crear")
    public StudentModel CreateStudent(@RequestBody StudentModel studentModel){
        return studentService.createStudent(studentModel);
    }

}
