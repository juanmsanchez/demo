package co.edu.uniajc.demo.Model;
import javax.persistence.*;

@Entity
@Table(name = "student")
public class StudentModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column (name = "std_id")
    private long id;

    @Column (name = "std_name")
    private String name;

    @Column (name = "std_last_name")
    private String LastName;

    @Column (name = "std_age")
    private int  age;

    @Column (name = "std_state")
    private String state;

    public StudentModel() {

    }

    public StudentModel(long id, String name, String lastName, int age, String state) {
        this.id = id;
        this.name = name;
        this.LastName = lastName;
        this.age = age;
        this.state = state;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
